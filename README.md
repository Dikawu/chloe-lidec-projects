# Chloe Lidec projects

Here you can find all the projects I finished, wether at school or by myself.

## Dev Python

You can find in this directory:

- [ ] Sae1_exploit_csv : It's a project for school. We had to create multiple functions in order to intercat with a csv file. The functions were all tested and there's also one file designed to intercat with an user that wants informations. 
- [ ] tp10_matrices : It's a TP for school where we had multiple exercises including one to exploit matrixes.

## Hotels SQL

In this directory you'll find a tables creation file and a insert file. Both were done for school.

## Jean Zay NSI presentation

In this you can see a website I created in 1ère with Laura Leroy. It was done to present our school and the NSI subject.

## SiteSupergirlLidec

In this directory, you can see a website I did as part of my first semester in BUT Informatique. It's a website about the show called Supergirl and three pages are completed.

## Système

In this directory, you can find an archive with few files in bash that I made for a school project which consisted of creating a coding environement on a computer.

## Cyber attack at IUT'O

In this directory, you can find all the files needed to launch the "cyber_attack_at_iuto.py" file in the source repretory. It's a little trojan game I created along with two of my classmates for a project in my first year of BUT Informatique.

## Pendu

In this directory, you can find all the files needed to launch the Pendu.java which is an application based on the game Hangman. I created this application during
my first year of BUT Informatique.

## Demineur

In this directory, you can find all the files needed to launch the DemineurGraphique.java which is an application of the game Mines.
I coded this game along with three classmates for a school project.

## Allo45 Application

It's an application that I developped along with four classmates for a imaginary company. There's two modules, one for an analyst and one for a pollster. You can't launch the app directly, you'll need to launch a podman first and load the database with the creation files. 
